package com.jaom.ws.soap;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.jaom.ws.soap.JDBC.ConexionMySQL;
import com.jaom.ws.trainings.DeleteOrdersResponse;
import com.jaom.ws.trainings.DeleteOrdersRequest;

import com.jaom.ws.trainings.CustomerOrdersPortType;

public class CustomerOrdersWsImpl implements CustomerOrdersPortType {

    @Override
    public DeleteOrdersResponse deleteOrders(DeleteOrdersRequest parameters)  {
        String username = parameters.getUsername();
        String password = parameters.getPassword();
        ConexionMySQL sql = new ConexionMySQL();
        Connection conn = sql.conectarMySQL();

        DeleteOrdersResponse response = new DeleteOrdersResponse();

        try {
            PreparedStatement pstm = conn.prepareStatement(
                    "SELECT * FROM user WHERE name = ? AND password = ?"
            );
            pstm.setString(1,username);
            pstm.setString(2,password);
            ResultSet result = pstm.executeQuery();
            if(result.first())
                response.setResult(true);
            else
                response.setResult(false);
        } catch (SQLException e) {
            e.printStackTrace();

        }

        return response;
    }
}