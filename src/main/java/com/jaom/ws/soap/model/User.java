package com.jaom.ws.soap.model;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class User {
    @Id
    private int idcredentials;
    private String password;
    private String name;

    public User(){}

    public User(int idcredentials, String password, String name) {
        this.idcredentials = idcredentials;
        this.password = password;
        this.name = name;
    }

    public int getIdcredentials() {
        return idcredentials;
    }

    public void setIdcredentials(int idcredentials) {
        this.idcredentials = idcredentials;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
