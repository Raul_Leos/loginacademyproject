package com.jaom.ws.soap.repository;

import com.jaom.ws.soap.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CredentialsRepository extends JpaRepository<User,Integer> {
    List<User> findByName(String name);
}
